#!/bin/sh

if [ "$(id -u)" = "0" ]; then
	echo "Do not run this script as root" 1>&2
	exit 1
fi

install_packages() {
	cur_dir=$(pwd)
	pkgsfile="packages.list"

	[ -f "$cur_dir/$pkgsfile" ] && sed -e '/^#/d' -e '/^$/d' "$cur_dir/$pkgsfile" > /tmp/$pkgsfile
	echo "\n>>> Installing packages ... <<<"
	xargs -a /tmp/$pkgsfile doas xbps-install
}

install_prezto() {
	if [ ! -d "$HOME/.zprezto" ]; then
		git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
		setopt EXTENDED_GLOB
		for rcfile in "${ZDOTDIR:-$HOME}/.zprezto/runcoms/^README.md(.N)"; do
			ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
		done
	else
		echo ">>> Prezto already installed <<<"
	fi
}

install_zim() {
	if [ ! -d "$HOME/.zim" ]; then
		echo "\n>>> Installing Zsh configuration framework - zimfw ... <<<"
		curl -fsSL https://raw.githubusercontent.com/zimfw/install/master/install.zsh | zsh
		echo 'if [ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zshrc" ]; then' >> $HOME/.zshrc
		echo '\tsource "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zshrc"' >> $HOME/.zshrc
		echo 'else' >> $HOME/.zshrc
		echo '\tprint "Note: ~/.config/zsh/zshrc is unavailable."' >> $HOME/.zshrc
		echo 'fi' >> $HOME/.zshrc
	else
		echo "\n>>> Zimfw already installed. <<<"
	fi
}

install_base16_shell() {
	if [ ! -d "${XDG_CONFIG_HOME:-$HOME/.config}"/base16-shell ]; then
		echo "\n>>> Installing base16-shell ... <<<"
		git clone https://github.com/chriskempson/base16-shell.git $HOME/.config/base16-shell
	else
		echo "\n>>> base16-shell already installed. <<<"
	fi
}

install_vim_plug() {
	if [ ! -f "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim ]; then
		echo "\n>>> Installing vim-plug ... <<<"
		sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
			https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	else
		echo "\n>>> vim-plug already installed. <<<"
	fi
}

install_lscolors() {
	if [ ! -f "${XDG_DATA_HOME:-$HOME/.local/share}"/lscolors.sh ]; then
		echo "\n>>> Installing LS_COLORS ... <<<"
		mkdir /tmp/LS_COLORS && curl -L https://api.github.com/repos/trapd00r/LS_COLORS/tarball/master | tar xzf - --directory=/tmp/LS_COLORS --strip=1
		( cd /tmp/LS_COLORS && make install )
	else
		echo "\n>>> LS_COLORS already installed. <<<"
	fi
}

set_x11() {
	if [ ! -d /etc/X11/xorg.conf.d ]; then
		echo "\n>>> Copying xorg.conf.d to /etc/X11 ... <<<"
		doas cp -r "$HOME/.config/x11/xorg.conf.d" /etc/X11
	else
		echo "\n>>> /etc/X11/xorg.conf.d existed. <<<"
	fi
}

install_packages
install_zim
install_base16_shell
install_vim_plug
install_lscolors
#set_x11
